import { Config } from "./config/config";
import express from "express";
import { Request, Response, NextFunction } from "express";
const config = new Config();
require("dotenv").config();
const LABEL = config.serviceName;

import {
  OPCUAClient,
  MessageSecurityMode,
  SecurityPolicy,
  AttributeIds,
  makeBrowsePath,
  ClientSubscription,
  TimestampsToReturn,
  MonitoringParametersOptions,
  ClientMonitoredItem,
  DataValue,
  Variant,
} from "node-opcua";
import * as http from "http";
var expressValidator: any = require("express-validator");
const app: any = express();
const server: any = http.createServer(app);
const port: any = config.port;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.set("port", port);

// Express Validator
app.use(
  expressValidator({
    errorFormatter: function (param: any, msg: any, value: any): any {
      var namespace: any = param.split("."),
        root: any = namespace.shift(),
        formParam: any = root;

      while (namespace.length) {
        formParam += "[" + namespace.shift() + "]";
      }
      return {
        param: formParam,
        msg: msg,
        value: value,
      };
    },
  })
);

app.use(express.static("public"));

app.use(function (req: any, res: any, next: any): any {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, Authorization, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST,PUT, DELETE");
  next();
});

//Test Route(ping)
app.use("/v1/test", async (req: any, res: any, next: any) => {
  try {
    let body: any = req.body.inputData;
    let resp: any = await getData(body);
    return res.status(200).json({
      status: 200,
      message: resp,
    });
  } catch (err) {
    return res.status(400).json({
      status: 400,
      message: "Error",
    });
  }
});

server.listen(port, () => {
  console.log("Running on port:", port);
});

opc();

process.on("SIGINT", function () {
  process.exit(0);
});

process.on("SIGTERM", function () {
  process.exit(0);
});

async function opc() {
  const connectionStrategy = {
    initialDelay: 1000,
    maxRetry: 1,
  };
  const endpointUrl = "opc.tcp://DESKTOP-KDQJ6JN:4334/UA/MyLittleServer";
  const client = OPCUAClient.create({
    endpointMustExist: false,
    applicationName: "MyClient",
    connectionStrategy: connectionStrategy,
    securityMode: MessageSecurityMode.None,
    securityPolicy: SecurityPolicy.None,
  });
  await client.connect(endpointUrl);
  const session = await client.createSession();

  const browseResult = await session.browse("ns=1;s=free_memory");
  if (null != browseResult["references"]) {
    console.log("references of RootFolder :");
    for (const reference of browseResult["references"]) {
      console.log("   -> ", reference.browseName.toString());
    }
  }

  const maxAge = 0;
  const nodeToRead = {
    nodeId: "ns=1;s=free_memory",
    attributeId: AttributeIds.Value,
  };

  const dataValue = await session.read(nodeToRead, maxAge);
  console.log("Value:", dataValue.value.value);

  const subscription = ClientSubscription.create(session, {
    requestedPublishingInterval: 1000,
    requestedLifetimeCount: 100,
    requestedMaxKeepAliveCount: 10,
    maxNotificationsPerPublish: 100,
    publishingEnabled: true,
    priority: 10,
  });

  subscription
    .on("started", function () {
      console.log(
        "subscription started for 2 seconds - subscriptionId=",
        subscription.subscriptionId
      );
    })
    .on("keepalive", function () {
      console.log("keepalive");
    })
    .on("terminated", function () {
      console.log("terminated");
    });

  // install monitored item

  const itemToMonitor = {
    nodeId: "ns=1;b=1020FFAA",
    attributeId: AttributeIds.Value,
  };
  const parameters: MonitoringParametersOptions = {
    samplingInterval: 100,
    discardOldest: true,
    queueSize: 10,
  };

  const monitoredItem = ClientMonitoredItem.create(
    subscription,
    itemToMonitor,
    parameters,
    TimestampsToReturn.Both
  );

  monitoredItem.on("changed", (dataValue: DataValue) => {
    console.log("Value has changed:", dataValue.value.value);
  });

  async function timeout(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
  await timeout(20000);

  console.log("now terminating subscription");
  await subscription.terminate();

  await session.close();
  await client.disconnect();
}

async function getData(data) {
  const connectionStrategy = {
    initialDelay: 1000,
    maxRetry: 1,
  };
  const endpointUrl = "opc.tcp://DESKTOP-KDQJ6JN:4334/UA/MyLittleServer";
  const client = OPCUAClient.create({
    endpointMustExist: false,
    applicationName: "MyClient",
    connectionStrategy: connectionStrategy,
    securityMode: MessageSecurityMode.None,
    securityPolicy: SecurityPolicy.None,
  });
  await client.connect(endpointUrl);
  const session = await client.createSession();

  //   const browseResult = await session.browse("RootFolder");
  //   if (null != browseResult["references"]) {
  //      console.log("references of RootFolder :");
  //     for (const reference of browseResult["references"]) {
  //       console.log("   -> ", reference.browseName.toString());
  //     }
  //   }

  const maxAge = 0;
  const nodeToRead = {
    nodeId: data,
    attributeId: AttributeIds.Value,
  };
  const dataValue = await session.read(nodeToRead, maxAge);

  await session.close();
  await client.disconnect();

  return dataValue.value.value;
}

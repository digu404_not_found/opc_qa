const request = require('request');

const promisifiedRequest = function(options) {
  return new Promise((resolve,reject) => {
    request(options, (error, response, body) => {
      if (response) {
        return resolve(response);
      }
      if (error) {
        return reject(error);
      }
    });
  });
};

(async function() {
  const options = {
    url: 'http://localhost:8000/v1/allData?endPointUrl=opc.tcp://192.168.0.1:4840',
    method: 'GET',
    gzip: true,
    
  };

  let response = await promisifiedRequest(options);

 
  console.log(response.body,"hlow");
})();
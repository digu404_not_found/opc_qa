import { Config } from "./config/config";
import express from "express";
import { Request, Response, NextFunction } from "express";
const config = new Config();
require("dotenv").config();
const LABEL = config.serviceName;

import {
  OPCUAClient,
  MessageSecurityMode,
  SecurityPolicy,
  AttributeIds,
  makeBrowsePath,
  ClientSubscription,
  TimestampsToReturn,
  MonitoringParametersOptions,
  ClientMonitoredItem,
  DataValue,
  Variant,
  DataType,
} from "node-opcua";
import * as http from "http";
var expressValidator: any = require("express-validator");
const app: any = express();
const server: any = http.createServer(app);
const port: any = config.port;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.set("port", port);

// Express Validator
app.use(
  expressValidator({
    errorFormatter: function (param: any, msg: any, value: any): any {
      var namespace: any = param.split("."),
        root: any = namespace.shift(),
        formParam: any = root;

      while (namespace.length) {
        formParam += "[" + namespace.shift() + "]";
      }
      return {
        param: formParam,
        msg: msg,
        value: value,
      };
    },
  })
);

app.use(express.static("public"));

app.use(function (req: any, res: any, next: any): any {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, Authorization, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST,PUT, DELETE");
  next();
});

//Test Route(ping)
app.get("/v1/allData", async (req: any, res: any, next: any) => {
  try {
    let resp: any = await opc(); 
    console.log("testttting____________",resp);
    return res.status(200).json({
      status: 200,
      message: resp,
    });
  } catch (err) {
    return res.status(400).json({
      status: 400,
      message: "Error",
    });
  }
});

app.post("/v1/data", async (req: any, res: any, next: any) => {
  try {
    let body: any = req.body;
    let resp: any = await modifyData(body);
    return res.status(200).json({
      status: 200,
      message: resp,
    });
  } catch (err) {
    return res.status(400).json({
      status: 400,
      message: "Error",
    });
  }
});

server.listen(port, () => {
  console.log("Running on port:", port);
});

process.on("SIGINT", function () {
  process.exit(0);
});

process.on("SIGTERM", function () {
  process.exit(0);
});

async function opc() {
  let data: any = {};
  const connectionStrategy = {
    initialDelay: 1000,
    maxRetry: 1,
  };
  const endpointUrl = config.endpointUrl;
  const client = OPCUAClient.create({
    endpointMustExist: false,
    applicationName: "MyClient",
    connectionStrategy: connectionStrategy,
    securityMode: MessageSecurityMode.None,
    securityPolicy: SecurityPolicy.None,
  });
  await client.connect(endpointUrl);
  const session = await client.createSession();

  const browseResult = await session.browse("RootFolder");
  // console.log("****************",browseResult);
  if (null != browseResult.references) {
    // console.log("&&&&&&&&&&&&&",browseResult.references)
    for (const reference of browseResult.references) {
      if (reference.browseName.toString() == "Objects") {
        let referenceVal = await getVal(session, reference.nodeId.toString());
        // console.log(
        //   "-> ",
        //   reference.browseName.toString().slice(2),
        //   reference.nodeId.toString(),
        //   referenceVal.value
        // );
        let device2Tag = await session.browse(reference.nodeId.toString());
        // console.log("...............",device2Tag)
        if (null != device2Tag.references) {
          for (let reference1 of device2Tag.references) {
            if (
              reference1.browseName.toString() != "Aliases" &&
              reference1.browseName.toString() != "Server"
            ) {
                   
              console.log("...............",device2Tag.references)
              let referenceVal1 = await getVal(
                session,
                reference1.nodeId.toString()
              );
              data[reference1.browseName.toString().slice(2)] = {};
              data[reference1.browseName.toString().slice(2)]["tag"] = {};
              data[reference1.browseName.toString().slice(2)]["folder"] = {};
              data[reference1.browseName.toString().slice(2)]["nodeId"] =
                reference1.nodeId.toString();
                // console.log("directories:", data);
              // if (
              //   referenceVal1 &&
              //   referenceVal1.value &&
              //   referenceVal1.value.dataType
              // ) {
              //   data['tag'][reference1.browseName.toString().slice(2)] =
              //     referenceVal1.value.toJSON();
              // }

              // console.log(
              //   "    -> ",
              //   reference1.browseName.toString().slice(2),
              //   reference1.nodeId.toString(),
              //   referenceVal1.value
              // );
              const device3Tag = await session.browse(
                reference1.nodeId.toString()
              );
              // console.log("-----",device3Tag.references);
              if (null != device3Tag.references) {
                
                for (let reference2 of device3Tag.references) {
                  let referenceVal2 = await getVal(
                    session,
                    reference2.nodeId.toString()
                  );
                  if (
                    referenceVal2 &&
                    referenceVal2.value &&
                    referenceVal2.value.dataType
                  ) {
                    data[reference1.browseName.toString().slice(2)]["tag"][
                      reference2.browseName.toString().slice(2)
                    ] = {
                      nodeId: reference2.nodeId.toString(),
                      ...referenceVal2.value.toJSON(),
                    };
                  } else {
                    data[reference1.browseName.toString().slice(2)]["folder"][
                      reference2.browseName.toString().slice(2)
                    ] = {};
                    data[reference1.browseName.toString().slice(2)]["folder"][
                      reference2.browseName.toString().slice(2)
                    ]["tag"] = {};
                    data[reference1.browseName.toString().slice(2)]["folder"][
                      reference2.browseName.toString().slice(2)
                    ]["folder"] = {};
                    data[reference1.browseName.toString().slice(2)]["folder"][
                      reference2.browseName.toString().slice(2)
                    ]["nodeId"] = reference2.nodeId.toString();
                  }

                  // console.log(
                  //   "        -> ",
                  //   reference2.browseName.toString().slice(2),
                  //   reference2.nodeId.toString(),
                  //   referenceVal2.value
                  // );
                  const device4Tag = await session.browse(
                    reference2.nodeId.toString()
                  );
                  if (null != device4Tag.references) {
                    for (let reference3 of device4Tag.references) {
                      let referenceVal3 = await getVal(
                        session,
                        reference3.nodeId.toString()
                      );

                      if (
                        referenceVal3 &&
                        referenceVal3.value &&
                        referenceVal3.value.dataType
                      ) {
                        data[reference1.browseName.toString().slice(2)][
                          "folder"
                        ][reference2.browseName.toString().slice(2)]["tag"][
                          reference3.browseName.toString().slice(2)
                        ] = {
                          nodeId: reference3.nodeId.toString(),
                          ...referenceVal3.value.toJSON(),
                        };
                      } else {
                        data[reference1.browseName.toString().slice(2)][
                          "folder"
                        ][reference2.browseName.toString().slice(2)]["folder"][
                          reference3.browseName.toString().slice(2)
                        ] = {};
                        data[reference1.browseName.toString().slice(2)][
                          "folder"
                        ][reference2.browseName.toString().slice(2)]["folder"][
                          reference3.browseName.toString().slice(2)
                        ]["tag"] = {};
                        data[reference1.browseName.toString().slice(2)][
                          "folder"
                        ][reference2.browseName.toString().slice(2)]["folder"][
                          reference3.browseName.toString().slice(2)
                        ]["folder"] = {};
                        data[reference1.browseName.toString().slice(2)][
                          "folder"
                        ][reference2.browseName.toString().slice(2)]["folder"][
                          reference3.browseName.toString().slice(2)
                        ]["nodeId"] = reference3.nodeId.toString();
                      }

                      // console.log(
                      //   "            -> ",
                      //   reference3.browseName.toString().slice(2),
                      //   reference3.nodeId.toString(),
                      //   referenceVal3.value
                      // );
                      const device5Tag = await session.browse(
                        reference3.nodeId.toString()
                      );
                      if (null != device5Tag.references) {
                        for (let reference4 of device5Tag.references) {
                          let referenceVal4 = await getVal(
                            session,
                            reference4.nodeId.toString()
                          );
                          if (
                            referenceVal4 &&
                            referenceVal4.value &&
                            referenceVal4.value.dataType
                          ) {
                            data[reference1.browseName.toString().slice(2)][
                              "folder"
                            ][reference2.browseName.toString().slice(2)][
                              "folder"
                            ][reference3.browseName.toString().slice(2)]["tag"][
                              reference4.browseName.toString().slice(2)
                            ] = {
                              nodeId: reference4.nodeId.toString(),
                              ...referenceVal4.value.toJSON(),
                            };
                          } else {
                            data[reference1.browseName.toString().slice(2)][
                              "folder"
                            ][reference2.browseName.toString().slice(2)][
                              "folder"
                            ][reference3.browseName.toString().slice(2)][
                              "folder"
                            ][reference4.browseName.toString().slice(2)] = {};
                            data[reference1.browseName.toString().slice(2)][
                              "folder"
                            ][reference2.browseName.toString().slice(2)][
                              "folder"
                            ][reference3.browseName.toString().slice(2)][
                              "folder"
                            ][reference4.browseName.toString().slice(2)][
                              "tag"
                            ] = {};
                            data[reference1.browseName.toString().slice(2)][
                              "folder"
                            ][reference2.browseName.toString().slice(2)][
                              "folder"
                            ][reference3.browseName.toString().slice(2)][
                              "folder"
                            ][reference4.browseName.toString().slice(2)][
                              "folder"
                            ] = {};
                            data[reference1.browseName.toString().slice(2)][
                              "folder"
                            ][reference2.browseName.toString().slice(2)][
                              "folder"
                            ][reference3.browseName.toString().slice(2)][
                              "folder"
                            ][reference4.browseName.toString().slice(2)][
                              "nodeId"
                            ] = reference4.nodeId.toString();
                          }

                          // console.log(
                          //   "                -> ",
                          //   reference4.browseName.toString().slice(2),
                          //   reference4.nodeId.toString(),
                          //   referenceVal4.value
                          // );


                          // checking area
                          
                          const device6Tag = await session.browse(
                            reference4.nodeId.toString()
                          );
                          if (null != device6Tag.references) {
                            for (let reference5 of device6Tag.references) {
                              let referenceVal5 = await getVal(
                                session,
                                reference5.nodeId.toString()
                              );
                              if (
                                referenceVal5 &&
                                referenceVal5.value &&
                                referenceVal5.value.dataType
                              ) {
                                data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["tag"][reference5.browseName.toString().slice(2)] = {nodeId: reference5.nodeId.toString(),
                                  ...referenceVal5.value.toJSON(),
                                };
                              } else {
                                data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)] = {};
                                data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["tag"] = {};
                                data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"] = {};
                                data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["nodeId"] = reference5.nodeId.toString();
                              }

                              const device7Tag = await session.browse(
                                reference5.nodeId.toString()
                              );
                              if (null != device7Tag.references) {
                                for (let reference6 of device7Tag.references) {
                                  let referenceVal6 = await getVal(
                                    session,
                                    reference6.nodeId.toString()
                                  );
                                  if (
                                    referenceVal6 &&
                                    referenceVal6.value &&
                                    referenceVal6.value.dataType
                                  ) {
                                    data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["tag"][reference6.browseName.toString().slice(2)] = {nodeId: reference6.nodeId.toString(),
                                      ...referenceVal6.value.toJSON(),
                                    };
                                  } else {
                                    data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)] = {};
                                    data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)]["tag"] = {};
                                    data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)]["folder"] = {};
                                    data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)]["nodeId"] = reference6.nodeId.toString();
                                  }
    
    
                                  // 8th one

                                  const device8Tag = await session.browse(
                                    reference6.nodeId.toString()
                                  );
                                  if (null != device8Tag.references) {
                                    for (let reference7 of device8Tag.references) {
                                      let referenceVal7 = await getVal(
                                        session,
                                        reference7.nodeId.toString()
                                      );
                                      if (
                                        referenceVal7 &&
                                        referenceVal7.value &&
                                        referenceVal7.value.dataType
                                      ) {
                                        data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)]["tag"][reference7.browseName.toString().slice(2)] = {nodeId: reference7.nodeId.toString(),
                                          ...referenceVal7.value.toJSON(),
                                        };
                                      } else {
                                        data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)]["folder"][reference7.browseName.toString().slice(2)] = {};
                                        data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)]["folder"][reference7.browseName.toString().slice(2)]["tag"] = {};
                                        data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)]["folder"][reference7.browseName.toString().slice(2)]["folder"] = {};
                                        data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)]["folder"][reference7.browseName.toString().slice(2)]["nodeId"] = reference7.nodeId.toString();
                                      }
        
        
                                      // start of 9th one

                                      // const device9Tag = await session.browse(
                                      //   reference7.nodeId.toString()
                                      // );
                                      // if (null != device9Tag.references) {
                                      //   for (let reference8 of device9Tag.references) {
                                      //     let referenceVal8 = await getVal(
                                      //       session,
                                      //       reference8.nodeId.toString()
                                      //     );
                                      //     if (
                                      //       referenceVal8 &&
                                      //       referenceVal8.value &&
                                      //       referenceVal8.value.dataType
                                      //     ) {
                                      //       data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)]["folder"][reference7.browseName.toString().slice(2)]["tag"][reference8.browseName.toString().slice(2)] = {nodeId: reference8.nodeId.toString(),
                                      //         ...referenceVal8.value.toJSON(),
                                      //       };
                                      //     } else {
                                      //       data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)]["folder"][reference7.browseName.toString().slice(2)]["folder"][reference8.browseName.toString().slice(2)] = {};
                                      //       data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)]["folder"][reference7.browseName.toString().slice(2)]["folder"][reference8.browseName.toString().slice(2)]["tag"] = {};
                                      //       data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)]["folder"][reference7.browseName.toString().slice(2)]["folder"][reference8.browseName.toString().slice(2)]["folder"] = {};
                                      //       data[reference1.browseName.toString().slice(2)]["folder"][reference2.browseName.toString().slice(2)]["folder"][reference3.browseName.toString().slice(2)]["folder"][reference4.browseName.toString().slice(2)]["folder"][reference5.browseName.toString().slice(2)]["folder"][reference6.browseName.toString().slice(2)]["folder"][reference7.browseName.toString().slice(2)]["folder"][reference8.browseName.toString().slice(2)]["nodeId"] = reference8.nodeId.toString();
                                      //     }
            
                                      //   }
                                      // }
                                      // end of 9th one
                                    }
                                  }
                                  // end of 8th one
                                }
                              }
                            }
                          }
                          // end checking

                        }
                      }
                     

                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  await session.close();
  await client.disconnect();
  console.log("?????????",data);
  return data;
}

async function getVal(session: any, id: any) {
  const maxAge = 0;
  const nodeToRead = {
    nodeId: id,
    attributeId: AttributeIds.Value,
  };
  return await session.read(nodeToRead, maxAge);
}

async function modifyData(body: any) {
  const connectionStrategy = {
    initialDelay: 1000,
    maxRetry: 1,
  };
  const endpointUrl = config.endpointUrl;
  const client = OPCUAClient.create({
    endpointMustExist: false,
    applicationName: "MyClient",
    connectionStrategy: connectionStrategy,
    securityMode: MessageSecurityMode.None,
    securityPolicy: SecurityPolicy.None,
  });
  await client.connect(endpointUrl);
  const session = await client.createSession();
  var nodesToWrite: any = {
    nodeId: body.nodeId,
    attributeId: AttributeIds.Value,
    indexRange: null,
    value: {
      value: {
        dataType: DataType[body.dataType],
        value: body.value,
      },
    },
  };
  let resp: any = await session.write(nodesToWrite);
  await session.close();
  await client.disconnect();
  return resp;
}

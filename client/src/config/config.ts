require("dotenv").config();

export class Config {
  serviceName = process.env.SERVICE_NAME || "OPCUA_CLIENT";
  endpointUrl =
    process.env.ENDPOINT_URL || "opc.tcp://laptop-2ggi43kn:4840/";
  port = process.env.PORT || 8000;
}
